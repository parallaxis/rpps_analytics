# ==========================================================
# Filter Features
#
# Este scprit encontra todos os entes com o perfil desejado.
# O perfil desejado é composto pelas features.
#
# ==========================================================
#
# ============== Support Functions ========================
# Este funcao formata o arquivo corrigindo alguns erros 
#source("init.R")
#source("utils.R")

# =================== Preparing the file ======================
#rppsFileRaw <- read.csv("../dataset/dados_tudo.csv", sep = "|")
#rppsFile <- curateData(rppsFileRaw)

# =================== Filtering System ===============================

cluster <- function(file, features, valor, operador){
  # Selecting entes just from specifically states
  #index <- likeFunction(file, pattern, field)
  #fileState <- file[index, ]
    
  # Evaluating each features separetely
  indexComb <- matrix(data = FALSE, dim(file)[1], length(features))
  # This loop find all observations from each feature accordingly its desirable value
  for(i in 1:length(features)){  
    if(operador[i] == ">"){
      indexComb[,i] <- file[,features[i]] > valor[i]
    } 
    else if(operador[i] == "<"){
      indexComb[,i] <- file[,features[i]] < valor[i]
    } 
    else{
      #indexComb[,i] <- file[,features[i]] == valor[i]
      index <- likeFunction(file, valor[i], features[i])
      indexComb[index,i] <- TRUE
    } 
  }
  # Find index where all features are TRUE 
  index <- apply(indexComb, c(1), all) & complete.cases(indexComb)
  #index <- apply(indexComb, c(1), all)   
  #Getting the clusters
  clust <- file[index,] 
  return(clust)
}








